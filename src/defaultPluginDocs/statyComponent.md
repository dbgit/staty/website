A component exists of 2 main parts; a definition and a slot

#### The Definition

A component definition can be written in html, json or yaml

It can include the following things:
* Name
* List of Props

#### The Slot

Here goes normal html...

#### Example
```html
<component>
  <definition>
    <meta name="Name" content="Example"/>
    <meta name="prop" content="first-name" default="Foobar"/>
  </definition>
  <slot>
    <p>Hello {{ firstname }}</p>
  </slot>
</component>
```
