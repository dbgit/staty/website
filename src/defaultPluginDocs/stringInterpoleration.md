This plugins allows you to use variables in strings by wrapping the variable between curly braces.

#### Example
```html
<p>Hello {{ someVariable }}</p>
```
