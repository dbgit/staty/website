This plugin allows you to use variables in side of attributes by prefixing the attribute with a colon

#### Example
```html
<p :class="someVariable">Hello World</p>
```
