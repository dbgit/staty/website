The markdown parsers hooks itself into the `<parse>` tag, you can use it by specifying the `lang` attribute.

#### Example
```html
<parse lang="markdown">
  # Hello World
  
  This is markdown inside a staty component
</parse>
```
