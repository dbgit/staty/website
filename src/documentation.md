# Staty

Staty is a fully modular static site compiler for small to medium websites that incorporates modern frontend paradigms

## Full Example

### `index.html`
```html
<html>
  <head>
    <title>Hello World</title>
  </head>
  <body>
    <parse lang="md">
      # A cool title  
    </parse>
    <p>Some cool text</p>
    <component path="./hello.staty" first-name="world"></component>
  </body>
</html>
```

### `hello.staty`
```html
<component>
  <definition>
    <meta name="Name" content="Example"/>
    <meta name="prop" content="first-name" default="Foobar"/>
  </definition>
  <slot>
    <p>Hello {{ firstname }}</p>
  </slot>
</component>
```


## Motivation

TL;DR: There is no good and modern way to write a static website with reusable components, without a big JS runtime

<details>
<summary>Full Story</summary>

It all started when i was looking a good way to rewrite my personal website, it had to be fast and with good SEO.

While trying to find a good tool i evaluated the following:
* Frontend frameworks like react, vue or angular
  * \+ You can create re-usable components
  * \- They all depend javascript runtime which is bad for SEO
* Frontend frameworks with SSR
  * \+ Solves the SEO problem
  * \- I don't want to host a web server to render static content
* Templating languages like mustache and handlebars
  * \+ Easy to work with
  * \- No component properties
  * \- They feel dated if you work a lot with modern frontend frameworks
* Just write an index.html by hand
  * \+ Absolute freedom
  * \- No components (thus lots of copy and pasting)

Nothing really fitted what i needed, so i asked what my frontend designer friends use... They all use a php backend with mostly twig (or some other templating language)...

I think we can do better than that... and that is when staty was born

</details>

## Features

* Custom component properties
* Variables
* A Plugin system

## Default Plugins
