const Staty = require('@staty/staty').default;
const fs = require('fs');
const path = require('path');

const renderPage = (path, variables) => {
  const staty = new Staty({
    compiler: {
      globalVariables: variables
    }
  });
  return staty.compile(path);
};

(async () => {
  console.info('Starting...');
  console.time('Build Time');
  const defaultParams = {
    documentationFile: './documentation.md'
  };

  const styles = {
    water: {
      light: 'https://watercss.netlify.com/dist/light.standalone.css',
      dark: 'https://watercss.netlify.com/dist/dark.standalone.css'
    },
    highlight: {
      light: 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/github.min.css',
      dark: 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.10/styles/agate.min.css'
    }
  };

  const [light, dark] = await Promise.all([
    renderPage('./src/index.html', {
      ...defaultParams,
      themeLink: './index.html',
      themeLinkName: 'Dark',
      waterCss: styles.water.light,
      highlightCss: styles.highlight.light
    }),
    renderPage('./src/index.html', {
      ...defaultParams,
      themeLink: './light.html',
      themeLinkName: 'Light',
      waterCss: styles.water.dark,
      highlightCss: styles.highlight.dark
    })
  ]);


  const buildFilePath = path.resolve('./public/index.html');
  if(!fs.existsSync(path.dirname(buildFilePath))) {
    fs.mkdirSync(path.dirname(buildFilePath));
  }

  fs.writeFileSync(buildFilePath, dark);
  fs.writeFileSync(path.resolve(path.dirname(buildFilePath), './light.html'), light);
  console.log('Done...');
  console.timeEnd('Build Time');
})();
